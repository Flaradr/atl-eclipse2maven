<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="EMF2Maven"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchEclipse2Maven():V"/>
		<constant value="A.__matchPlugin2Dependency():V"/>
		<constant value="A.__matchBundle2Dependency():V"/>
		<constant value="__exec__"/>
		<constant value="Eclipse2Maven"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyEclipse2Maven(NTransientLink;):V"/>
		<constant value="Plugin2Dependency"/>
		<constant value="A.__applyPlugin2Dependency(NTransientLink;):V"/>
		<constant value="Bundle2Dependency"/>
		<constant value="A.__applyBundle2Dependency(NTransientLink;):V"/>
		<constant value="__matchEclipse2Maven"/>
		<constant value="PDEGraph"/>
		<constant value="emf"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="maven"/>
		<constant value="Model"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="dep"/>
		<constant value="DependenciesType2"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="14:3-22:4"/>
		<constant value="23:3-25:4"/>
		<constant value="__applyEclipse2Maven"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="4"/>
		<constant value="groupId"/>
		<constant value="4.0.0"/>
		<constant value="modelVersion"/>
		<constant value="1.0"/>
		<constant value="version"/>
		<constant value="description"/>
		<constant value="dependencies"/>
		<constant value="J.getEachPlugins(J):J"/>
		<constant value="dependency"/>
		<constant value="15:12-15:15"/>
		<constant value="15:12-15:20"/>
		<constant value="15:4-15:20"/>
		<constant value="16:15-16:18"/>
		<constant value="16:15-16:23"/>
		<constant value="16:4-16:23"/>
		<constant value="18:20-18:27"/>
		<constant value="18:4-18:27"/>
		<constant value="19:15-19:20"/>
		<constant value="19:4-19:20"/>
		<constant value="20:19-20:22"/>
		<constant value="20:19-20:34"/>
		<constant value="20:4-20:34"/>
		<constant value="21:20-21:23"/>
		<constant value="21:4-21:23"/>
		<constant value="24:18-24:28"/>
		<constant value="24:44-24:47"/>
		<constant value="24:18-24:48"/>
		<constant value="24:4-24:48"/>
		<constant value="link"/>
		<constant value="getEachPlugins"/>
		<constant value="Set"/>
		<constant value="J.getPlugins(J):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.getBundles(J):J"/>
		<constant value="J.getFeaturesPlugin(J):J"/>
		<constant value="J.getImportedPlugin(J):J"/>
		<constant value="32:3-32:13"/>
		<constant value="32:25-32:32"/>
		<constant value="32:3-32:33"/>
		<constant value="33:3-33:13"/>
		<constant value="33:25-33:32"/>
		<constant value="33:3-33:33"/>
		<constant value="34:3-34:13"/>
		<constant value="34:32-34:39"/>
		<constant value="34:3-34:40"/>
		<constant value="35:3-35:13"/>
		<constant value="35:32-35:39"/>
		<constant value="35:3-35:40"/>
		<constant value="31:2-36:3"/>
		<constant value="project"/>
		<constant value="getPlugins"/>
		<constant value="plugins"/>
		<constant value="42:2-42:9"/>
		<constant value="42:2-42:17"/>
		<constant value="getBundles"/>
		<constant value="bundles"/>
		<constant value="48:2-48:9"/>
		<constant value="48:2-48:17"/>
		<constant value="getFeaturesPlugin"/>
		<constant value="features"/>
		<constant value="J.notEmpty():J"/>
		<constant value="B.not():B"/>
		<constant value="J.flatten():J"/>
		<constant value="54:2-54:9"/>
		<constant value="54:2-54:18"/>
		<constant value="55:16-55:17"/>
		<constant value="55:16-55:25"/>
		<constant value="55:16-55:39"/>
		<constant value="54:2-55:40"/>
		<constant value="56:18-56:19"/>
		<constant value="56:18-56:27"/>
		<constant value="54:2-56:28"/>
		<constant value="54:2-57:15"/>
		<constant value="a"/>
		<constant value="getImportedPlugin"/>
		<constant value="importedPackages"/>
		<constant value="14"/>
		<constant value="63:2-63:9"/>
		<constant value="63:2-63:17"/>
		<constant value="64:18-64:19"/>
		<constant value="64:18-64:36"/>
		<constant value="64:18-64:50"/>
		<constant value="63:2-64:51"/>
		<constant value="63:2-65:15"/>
		<constant value="__matchPlugin2Dependency"/>
		<constant value="Plugin"/>
		<constant value="plugin"/>
		<constant value="Dependency"/>
		<constant value="72:3-75:4"/>
		<constant value="__applyPlugin2Dependency"/>
		<constant value="artifactId"/>
		<constant value="74:18-74:24"/>
		<constant value="74:18-74:29"/>
		<constant value="74:4-74:29"/>
		<constant value="__matchBundle2Dependency"/>
		<constant value="Bundle"/>
		<constant value="bundle"/>
		<constant value="83:3-87:4"/>
		<constant value="__applyBundle2Dependency"/>
		<constant value="J.getScope():J"/>
		<constant value="scope"/>
		<constant value="85:18-85:24"/>
		<constant value="85:18-85:29"/>
		<constant value="85:4-85:29"/>
		<constant value="86:13-86:19"/>
		<constant value="86:13-86:30"/>
		<constant value="86:4-86:30"/>
		<constant value="getScope"/>
		<constant value="Memf!Bundle;"/>
		<constant value="J.allInstances():J"/>
		<constant value="exportedBundles"/>
		<constant value="25"/>
		<constant value="J.toString():J"/>
		<constant value="IN!"/>
		<constant value="0"/>
		<constant value="J.+(J):J"/>
		<constant value="J.=(J):J"/>
		<constant value="59"/>
		<constant value="64"/>
		<constant value="compile"/>
		<constant value="65"/>
		<constant value="runtime"/>
		<constant value="93:5-93:15"/>
		<constant value="93:5-93:30"/>
		<constant value="94:18-94:19"/>
		<constant value="94:18-94:35"/>
		<constant value="94:18-94:47"/>
		<constant value="93:5-94:48"/>
		<constant value="95:18-95:19"/>
		<constant value="95:18-95:35"/>
		<constant value="95:52-95:53"/>
		<constant value="95:18-95:54"/>
		<constant value="93:5-95:55"/>
		<constant value="93:5-96:15"/>
		<constant value="97:18-97:19"/>
		<constant value="93:5-97:20"/>
		<constant value="98:18-98:19"/>
		<constant value="98:18-98:30"/>
		<constant value="98:33-98:38"/>
		<constant value="98:39-98:43"/>
		<constant value="98:39-98:48"/>
		<constant value="98:33-98:48"/>
		<constant value="98:18-98:48"/>
		<constant value="93:5-98:49"/>
		<constant value="93:5-98:63"/>
		<constant value="102:3-102:12"/>
		<constant value="100:3-100:12"/>
		<constant value="93:2-103:7"/>
		<constant value="b"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<operation name="5">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="7"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="9"/>
			<pcall arg="10"/>
			<dup/>
			<push arg="11"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<pcall arg="10"/>
			<pcall arg="13"/>
			<set arg="3"/>
			<getasm/>
			<push arg="14"/>
			<push arg="8"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="15"/>
			<getasm/>
			<pcall arg="16"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="18">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<getasm/>
			<get arg="3"/>
			<call arg="20"/>
			<if arg="21"/>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<dup/>
			<call arg="23"/>
			<if arg="24"/>
			<load arg="19"/>
			<call arg="25"/>
			<goto arg="26"/>
			<pop/>
			<load arg="19"/>
			<goto arg="27"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<iterate/>
			<store arg="29"/>
			<getasm/>
			<load arg="29"/>
			<call arg="30"/>
			<call arg="31"/>
			<enditerate/>
			<call arg="32"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="33" begin="23" end="27"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="34" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="35">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
			<parameter name="29" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="6"/>
			<lve slot="1" name="34" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="40"/>
			<getasm/>
			<pcall arg="41"/>
			<getasm/>
			<pcall arg="42"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="43">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="44"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="46"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="47"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="48"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="49"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="50"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="5" end="8"/>
			<lve slot="1" name="33" begin="15" end="18"/>
			<lve slot="1" name="33" begin="25" end="28"/>
			<lve slot="0" name="17" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="51">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="52"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="56"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="44"/>
			<pcall arg="57"/>
			<dup/>
			<push arg="53"/>
			<load arg="19"/>
			<pcall arg="58"/>
			<dup/>
			<push arg="59"/>
			<push arg="60"/>
			<push arg="59"/>
			<new/>
			<pcall arg="61"/>
			<dup/>
			<push arg="62"/>
			<push arg="63"/>
			<push arg="59"/>
			<new/>
			<pcall arg="61"/>
			<pusht/>
			<pcall arg="64"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="65" begin="19" end="24"/>
			<lne id="66" begin="25" end="30"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="53" begin="6" end="32"/>
			<lve slot="0" name="17" begin="0" end="33"/>
		</localvariabletable>
	</operation>
	<operation name="67">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="68"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="53"/>
			<call arg="69"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="59"/>
			<call arg="70"/>
			<store arg="71"/>
			<load arg="19"/>
			<push arg="62"/>
			<call arg="70"/>
			<store arg="72"/>
			<load arg="71"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="73"/>
			<dup/>
			<getasm/>
			<push arg="74"/>
			<call arg="30"/>
			<set arg="75"/>
			<dup/>
			<getasm/>
			<push arg="76"/>
			<call arg="30"/>
			<set arg="77"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="78"/>
			<call arg="30"/>
			<set arg="78"/>
			<dup/>
			<getasm/>
			<load arg="72"/>
			<call arg="30"/>
			<set arg="79"/>
			<pop/>
			<load arg="72"/>
			<dup/>
			<getasm/>
			<getasm/>
			<load arg="29"/>
			<call arg="80"/>
			<call arg="30"/>
			<set arg="81"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="82" begin="15" end="15"/>
			<lne id="83" begin="15" end="16"/>
			<lne id="84" begin="13" end="18"/>
			<lne id="85" begin="21" end="21"/>
			<lne id="86" begin="21" end="22"/>
			<lne id="87" begin="19" end="24"/>
			<lne id="88" begin="27" end="27"/>
			<lne id="89" begin="25" end="29"/>
			<lne id="90" begin="32" end="32"/>
			<lne id="91" begin="30" end="34"/>
			<lne id="92" begin="37" end="37"/>
			<lne id="93" begin="37" end="38"/>
			<lne id="94" begin="35" end="40"/>
			<lne id="95" begin="43" end="43"/>
			<lne id="96" begin="41" end="45"/>
			<lne id="65" begin="12" end="46"/>
			<lne id="97" begin="50" end="50"/>
			<lne id="98" begin="51" end="51"/>
			<lne id="99" begin="50" end="52"/>
			<lne id="100" begin="48" end="54"/>
			<lne id="66" begin="47" end="55"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="59" begin="7" end="55"/>
			<lve slot="4" name="62" begin="11" end="55"/>
			<lve slot="2" name="53" begin="3" end="55"/>
			<lve slot="0" name="17" begin="0" end="55"/>
			<lve slot="1" name="101" begin="0" end="55"/>
		</localvariabletable>
	</operation>
	<operation name="102">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<push arg="103"/>
			<push arg="8"/>
			<new/>
			<getasm/>
			<load arg="19"/>
			<call arg="104"/>
			<call arg="105"/>
			<getasm/>
			<load arg="19"/>
			<call arg="106"/>
			<call arg="105"/>
			<getasm/>
			<load arg="19"/>
			<call arg="107"/>
			<call arg="105"/>
			<getasm/>
			<load arg="19"/>
			<call arg="108"/>
			<call arg="105"/>
		</code>
		<linenumbertable>
			<lne id="109" begin="3" end="3"/>
			<lne id="110" begin="4" end="4"/>
			<lne id="111" begin="3" end="5"/>
			<lne id="112" begin="7" end="7"/>
			<lne id="113" begin="8" end="8"/>
			<lne id="114" begin="7" end="9"/>
			<lne id="115" begin="11" end="11"/>
			<lne id="116" begin="12" end="12"/>
			<lne id="117" begin="11" end="13"/>
			<lne id="118" begin="15" end="15"/>
			<lne id="119" begin="16" end="16"/>
			<lne id="120" begin="15" end="17"/>
			<lne id="121" begin="0" end="18"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="18"/>
			<lve slot="1" name="122" begin="0" end="18"/>
		</localvariabletable>
	</operation>
	<operation name="123">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<get arg="124"/>
		</code>
		<linenumbertable>
			<lne id="125" begin="0" end="0"/>
			<lne id="126" begin="0" end="1"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="1"/>
			<lve slot="1" name="122" begin="0" end="1"/>
		</localvariabletable>
	</operation>
	<operation name="127">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<get arg="128"/>
		</code>
		<linenumbertable>
			<lne id="129" begin="0" end="0"/>
			<lne id="130" begin="0" end="1"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="1"/>
			<lve slot="1" name="122" begin="0" end="1"/>
		</localvariabletable>
	</operation>
	<operation name="131">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<get arg="132"/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="124"/>
			<call arg="133"/>
			<call arg="134"/>
			<if arg="26"/>
			<load arg="29"/>
			<call arg="105"/>
			<enditerate/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="124"/>
			<call arg="105"/>
			<enditerate/>
			<call arg="135"/>
		</code>
		<linenumbertable>
			<lne id="136" begin="6" end="6"/>
			<lne id="137" begin="6" end="7"/>
			<lne id="138" begin="10" end="10"/>
			<lne id="139" begin="10" end="11"/>
			<lne id="140" begin="10" end="12"/>
			<lne id="141" begin="3" end="17"/>
			<lne id="142" begin="20" end="20"/>
			<lne id="143" begin="20" end="21"/>
			<lne id="144" begin="0" end="23"/>
			<lne id="145" begin="0" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="146" begin="9" end="16"/>
			<lve slot="2" name="146" begin="19" end="22"/>
			<lve slot="0" name="17" begin="0" end="24"/>
			<lve slot="1" name="122" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="147">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<get arg="124"/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="148"/>
			<call arg="133"/>
			<call arg="134"/>
			<if arg="149"/>
			<load arg="29"/>
			<call arg="105"/>
			<enditerate/>
			<call arg="135"/>
		</code>
		<linenumbertable>
			<lne id="150" begin="3" end="3"/>
			<lne id="151" begin="3" end="4"/>
			<lne id="152" begin="7" end="7"/>
			<lne id="153" begin="7" end="8"/>
			<lne id="154" begin="7" end="9"/>
			<lne id="155" begin="0" end="14"/>
			<lne id="156" begin="0" end="15"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="146" begin="6" end="13"/>
			<lve slot="0" name="17" begin="0" end="15"/>
			<lve slot="1" name="122" begin="0" end="15"/>
		</localvariabletable>
	</operation>
	<operation name="157">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="158"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="56"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="47"/>
			<pcall arg="57"/>
			<dup/>
			<push arg="159"/>
			<load arg="19"/>
			<pcall arg="58"/>
			<dup/>
			<push arg="81"/>
			<push arg="160"/>
			<push arg="59"/>
			<new/>
			<pcall arg="61"/>
			<pusht/>
			<pcall arg="64"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="161" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="159" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="162">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="68"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="159"/>
			<call arg="69"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="81"/>
			<call arg="70"/>
			<store arg="71"/>
			<load arg="71"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="163"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="164" begin="11" end="11"/>
			<lne id="165" begin="11" end="12"/>
			<lne id="166" begin="9" end="14"/>
			<lne id="161" begin="8" end="15"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="81" begin="7" end="15"/>
			<lve slot="2" name="159" begin="3" end="15"/>
			<lve slot="0" name="17" begin="0" end="15"/>
			<lve slot="1" name="101" begin="0" end="15"/>
		</localvariabletable>
	</operation>
	<operation name="167">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="168"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="56"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="49"/>
			<pcall arg="57"/>
			<dup/>
			<push arg="169"/>
			<load arg="19"/>
			<pcall arg="58"/>
			<dup/>
			<push arg="81"/>
			<push arg="160"/>
			<push arg="59"/>
			<new/>
			<pcall arg="61"/>
			<pusht/>
			<pcall arg="64"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="170" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="169" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="171">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="68"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="169"/>
			<call arg="69"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="81"/>
			<call arg="70"/>
			<store arg="71"/>
			<load arg="71"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="163"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<call arg="172"/>
			<call arg="30"/>
			<set arg="173"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="174" begin="11" end="11"/>
			<lne id="175" begin="11" end="12"/>
			<lne id="176" begin="9" end="14"/>
			<lne id="177" begin="17" end="17"/>
			<lne id="178" begin="17" end="18"/>
			<lne id="179" begin="15" end="20"/>
			<lne id="170" begin="8" end="21"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="81" begin="7" end="21"/>
			<lve slot="2" name="169" begin="3" end="21"/>
			<lve slot="0" name="17" begin="0" end="21"/>
			<lve slot="1" name="101" begin="0" end="21"/>
		</localvariabletable>
	</operation>
	<operation name="180">
		<context type="181"/>
		<parameters>
		</parameters>
		<code>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="158"/>
			<push arg="53"/>
			<findme/>
			<call arg="182"/>
			<iterate/>
			<store arg="19"/>
			<load arg="19"/>
			<get arg="183"/>
			<call arg="133"/>
			<call arg="134"/>
			<if arg="184"/>
			<load arg="19"/>
			<call arg="105"/>
			<enditerate/>
			<iterate/>
			<store arg="19"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<get arg="183"/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<call arg="105"/>
			<enditerate/>
			<call arg="105"/>
			<enditerate/>
			<call arg="135"/>
			<iterate/>
			<store arg="19"/>
			<load arg="19"/>
			<call arg="105"/>
			<enditerate/>
			<iterate/>
			<store arg="19"/>
			<load arg="19"/>
			<call arg="185"/>
			<push arg="186"/>
			<load arg="187"/>
			<get arg="38"/>
			<call arg="188"/>
			<call arg="189"/>
			<call arg="134"/>
			<if arg="190"/>
			<load arg="19"/>
			<call arg="105"/>
			<enditerate/>
			<call arg="133"/>
			<if arg="191"/>
			<push arg="192"/>
			<goto arg="193"/>
			<push arg="194"/>
		</code>
		<linenumbertable>
			<lne id="195" begin="12" end="14"/>
			<lne id="196" begin="12" end="15"/>
			<lne id="197" begin="18" end="18"/>
			<lne id="198" begin="18" end="19"/>
			<lne id="199" begin="18" end="20"/>
			<lne id="200" begin="9" end="25"/>
			<lne id="201" begin="31" end="31"/>
			<lne id="202" begin="31" end="32"/>
			<lne id="203" begin="35" end="35"/>
			<lne id="204" begin="28" end="37"/>
			<lne id="205" begin="6" end="39"/>
			<lne id="206" begin="6" end="40"/>
			<lne id="207" begin="43" end="43"/>
			<lne id="208" begin="3" end="45"/>
			<lne id="209" begin="48" end="48"/>
			<lne id="210" begin="48" end="49"/>
			<lne id="211" begin="50" end="50"/>
			<lne id="212" begin="51" end="51"/>
			<lne id="213" begin="51" end="52"/>
			<lne id="214" begin="50" end="53"/>
			<lne id="215" begin="48" end="54"/>
			<lne id="216" begin="0" end="59"/>
			<lne id="217" begin="0" end="60"/>
			<lne id="218" begin="62" end="62"/>
			<lne id="219" begin="64" end="64"/>
			<lne id="220" begin="0" end="64"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="146" begin="17" end="24"/>
			<lve slot="2" name="221" begin="34" end="36"/>
			<lve slot="1" name="146" begin="27" end="38"/>
			<lve slot="1" name="146" begin="42" end="44"/>
			<lve slot="1" name="221" begin="47" end="58"/>
			<lve slot="0" name="17" begin="0" end="64"/>
		</localvariabletable>
	</operation>
</asm>
