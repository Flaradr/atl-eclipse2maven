# Transformation d'un PDEGraph en fichier *.POM

This ATL project is made to mavenize an eclipse project.  
The input is a PDEGraph, a file that gives each plugin/bundle/feature used in a project.  
This graph is obtained by using the tool [pdedependencies2dot](https://github.com/diverse-project/pdedependencies2dot).

The output is a .POM file that gives each dependency for a project.
This allows to use a project outside of eclipse while using the same tools.  
If a plugin does not exists as an artifact the transform will still succeed, however
some depencendy will not be created.
